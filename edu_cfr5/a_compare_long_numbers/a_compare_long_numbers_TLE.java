package edu_cfr5;

import java.util.Scanner;
import java.math.BigInteger;

class Solution
{
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String a = s.nextLine();
		String b = s.nextLine();

		BigInteger inta = new BigInteger(a);
		BigInteger intb = new BigInteger(b);

		if (inta.compareTo(intb) == 1) {
			System.out.println(">");
		}
		else if (inta.compareTo(intb) == -1) {
			System.out.println("<");
		}
		else {
			System.out.println("=");
		}
		
	}
}