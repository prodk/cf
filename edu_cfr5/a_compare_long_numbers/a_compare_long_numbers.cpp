#include <string>
#include <iostream>
#include <cstdio>
using namespace std;

//#define DEBUG

int main()
{
#ifdef DEBUG
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(0);

    string a;
    cin >> a;
    string b;
    cin >> b;

    const int lena = (int)a.size();
    const int lenb = (int)b.size();

    int ia = 0;
    while (ia < lena && a[ia] == '0') ++ia;

    int ib = 0;
    while (ib < lenb && b[ib] == '0') ++ib;

    const int realLena = lena - ia;
    const int realLenb = lenb - ib;

    if (realLena < realLenb) cout << "<" << endl;
    else if (realLena > realLenb) cout << ">" << endl;
    else {
        int i = ia;
        int j = ib;
        int count = 0;
        while (i < lena && j < lenb && a[i] == b[j]) {
            ++i;
            ++j;
            ++count;
        }

        if (realLena == count) cout << "=" << endl;
        else if (a[i] < b[j]) cout << "<" << endl;
        else cout << ">" << endl;
    }

    return 0;
}