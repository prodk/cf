import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;

public final class Solution
{
	public static void main(String[] args) {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String a = null;
		try {
			a = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String b = null;
		try {
			b = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BigInteger inta = new BigInteger(a);
		BigInteger intb = new BigInteger(b);
		
		PrintWriter out = new PrintWriter(System.out, true);

		if (inta.compareTo(intb) == 1) {
			out.println(">");
		}
		else if (inta.compareTo(intb) == -1) {
			out.println("<");
		}
		else {
			out.println("=");
		}
		
	}
}