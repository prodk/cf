#include <vector>
#include <iostream>
#include <cstdio>
#include <climits>
#include <algorithm>
using namespace std;

#define DEBUG

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
#endif

    ios_base::sync_with_stdio(0);
	
	int n = 0;
	cin >> n;
	int m = 0;
	cin >> m;
	
	vector<int> vMin;
	for (int i = 0; i < n; ++i) {
		int curMin = INT_MAX;
		for (int j = 0; j < m; ++j) {
			int v = 0;
			cin >> v;
			curMin = min(v, curMin);
		}
		vMin.push_back(curMin);
	}
	
	const int res =*max_element(begin(vMin), end(vMin));
	cout << res << endl;

	return 0;
}