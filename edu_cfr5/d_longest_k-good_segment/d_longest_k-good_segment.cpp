#include <vector>
#include <cstdio>
#include <iostream>
#include <map>
using namespace std;

//#define DEBUG

int main()
{
#ifdef DEBUG
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(0);

    int n = 0;
    int k = 0;
    cin >> n >> k;

    vector<int> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }

    vector<pair<int, int>> state;
    map<int, int> d;

    int start = 0;
    int len = 0;
    for (int i = 0; i < n; ++i) {
        d[a[i]]++;
        ++len;
        while (d.size() > k) {
            d[a[start]]--;
            if (d[a[start]] <= 0) d.erase(a[start]);
            ++start;
            --len;
        }

        pair<int, int> curState(start, len);
        state.push_back(curState);
    }

    int resId = 0;
    int maxLen = state[resId].second;
    for (int i = 0; i < n; ++i) {
        if (maxLen < state[i].second) {
            maxLen = state[i].second;
            resId = i;
        }
    }

    const int sRes = state[resId].first + 1;
    const int eRes = sRes + state[resId].second - 1;
    cout << sRes << " " << eRes << endl;

    return 0;
}