#include <vector>
#include <iostream>
#include <cstdio>
#include <climits>
#include <algorithm>
#include <set>
#include <string>
#include <cstdlib>
using namespace std;

typedef vector<int> vi;
typedef pair<int, int> pii;

vector<string> v;
vector<vi> comp;
int n, m;
vi compSize;

int dr[] = { 1,0,-1,0 };
int dc[] = { 0,1, 0,-1 }; // S,E,N,W, neighbors

bool isValid(int r, int c)
{
    return r >= 0 && r < n && c >= 0 && c < m;
}

int dfs(int r, int c, int id)
{
    if (!isValid(r, c)) return 0;
    int size = 1;
    comp[r][c] = id;
    for (int d = 0; d < 4; ++d) {
        const int rNew = r + dr[d];
        const int cNew = c + dc[d];
        if (isValid(rNew, cNew) && comp[rNew][cNew] == -1 && v[rNew][cNew] == '.') {
            size += dfs(rNew, cNew, id);
        }
    }
    return size;
}

//#define DEBUG

int main()
{
#ifdef DEBUG
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
#endif

    ios_base::sync_with_stdio(0);

    cin >> n >> m;

    vi compRow(m, -1);

    for (int i = 0; i < n; ++i) {
        string row;
        cin >> row;
        v.push_back(row);
        comp.push_back(compRow);
    }

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (comp[i][j] == -1 && v[i][j] == '.') {
                const int curSize = dfs(i, j, compSize.size());
                compSize.push_back(curSize);
            }
        }
    }

    for (int r = 0; r < n; ++r) {
        for (int c = 0; c < m; ++c) {
            if (v[r][c] == '*') {
                set<int> checked;
                int res = 1;
                for (int d = 0; d < 4; ++d) {
                    const int rNew = r + dr[d];
                    const int cNew = c + dc[d];
                    if (isValid(rNew, cNew)) {
                        const int id = comp[rNew][cNew];
                        if (id != -1 && checked.find(id) == end(checked)) {
                            checked.insert(id);
                            res += compSize[id];
                        }
                    }
                }
                cout << res % 10;
            }
            else cout << v[r][c];
        }
        cout << endl;
    }

    return 0;
}