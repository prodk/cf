#include <vector>
#include <iostream>
#include <cstdio>
#include <climits>
#include <algorithm>
#include <map>
#include <string>
#include <cstdlib>
using namespace std;

typedef vector<int> vi;
typedef pair<int, int> pii;

//#define DEBUG

vector<string> v;
vector<bool> visited;
map<pii, int> a;
int n, m;

int dr[] = { 1,0,-1,0 };
int dc[] = { 0,1, 0,-1 }; // S,E,N,W, neighbors

int to1D(int r, int c)
{
    return r*m + c;
}

bool isValid(int r, int c)
{
    return r >= 0 && r < n && c >= 0 && c < m;
}

int dfs(int r, int c)
{
    if (!isValid(r, c)) return 0;
    int ans = 1;
    visited[to1D(r, c)] = true;
    for (int d = 0; d < 4; ++d) {
        const int rNew = r + dr[d];
        const int cNew = c + dc[d];
        if (isValid(rNew, cNew) && !visited[to1D(rNew, cNew)] && v[rNew][cNew] != '*') {
            ans += dfs(rNew, cNew);
        }
    }
    return ans;
}

int main()
{
#ifdef DEBUG
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
#endif

    ios_base::sync_with_stdio(0);

    cin >> n;
    cin >> m;

    visited.resize(n*m, false);

    for (int i = 0; i < n; ++i) {
        string row;
        cin >> row;
        v.push_back(row);
        for (int j = 0; j < m; ++j) {
            if (row[j] == '*') {
                a[make_pair(i, j)] = 0;
            }
        }
    }

    for (auto & ast : a) {
        fill(begin(visited), end(visited), false);
        ast.second = dfs(ast.first.first, ast.first.second);
    }

    for (int r = 0; r < n; ++r) {
        for (int c = 0; c < m; ++c) {
            pii p = make_pair(r, c);
            if (a.find(p) != end(a)) {
                cout << a[p] % 10;
            }
            else cout << v[r][c];
        }
        cout << endl;
    }

    return 0;
}