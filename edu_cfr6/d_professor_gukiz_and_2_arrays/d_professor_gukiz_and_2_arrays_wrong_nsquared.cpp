#include <iostream>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <algorithm>
using namespace std;

typedef long long ll;

int n, m;
ll s0, s1, v;

ll sum(const vector<int> & v)
{
    ll res = 0l;
    for (int i : v) res += i;

    return res;
}

int findNewV(const vector<int>& a, const vector<int>& b, int & iNew, int & jNew, int k)
{
    int kNew = k;
    ll s0New = s0;
    ll s1New = s1;
    iNew = 0;
    jNew = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            s0New = s0 - a[i] + b[j];
            s1New = s1 - b[j] + a[i];
            const ll vNew = abs(s0New - s1New);
            if (vNew < v) {
                v = vNew;
                iNew = i;
                jNew = j;
                kNew = k + 1;
            }
        }
    }
    return kNew;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < n; ++i) cin >> a[i];

    cin >> m;
    vector<int> b(m);
    for (int j = 0; j < m; ++j) cin >> b[j];

    s0 = sum(a);
    s1 = sum(b);
    v = abs(s0 - s1);

    int iNew = 0;
    int jNew = 0;
    int k = 0;
    k = findNewV(a, b, iNew, jNew, k);
    vector<pair<int, int>> res;
    if (k == 1) {
        res.push_back(make_pair(iNew, jNew));
        s0 = s0 - a[iNew] + b[jNew];
        s1 = s1 - b[jNew] + a[iNew];
        swap(a[iNew], b[jNew]);

        k = findNewV(a, b, iNew, jNew, k);
        if (k == 2) {
            res.push_back(make_pair(iNew, jNew));
        }

    }

    cout << v << endl;
    cout << k << endl;
    for (auto p : res) cout << p.first+1 << " " << p.second+1 << endl;

    return 0;
}