#include <iostream>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <map>
using namespace std;

typedef long long ll;

int n, m;
ll s0, s1, v;

int findNewV(const vector<int>& a, const vector<int>& b, int & iNew, int & jNew, int k)
{
    int kNew = k;
    ll s0New = s0;
    ll s1New = s1;
    iNew = 0;
    jNew = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            s0New = s0 - a[i] + b[j];
            s1New = s1 - b[j] + a[i];
            const ll vNew = abs(s0New - s1New);
            if (vNew < v) {
                v = vNew;
                iNew = i;
                jNew = j;
                kNew = k + 1;
            }
        }
    }
    return kNew;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
        s0 += a[i];
    }

    cin >> m;
    vector<int> b(m);
    for (int j = 0; j < m; ++j) {
        cin >> b[j];
        s1 += b[j];
    }

    v = abs(s0 - s1);

    int iNew = 0;
    int jNew = 0;
    int k = 0;
    k = findNewV(a, b, iNew, jNew, k);
    vector<pair<int, int>> res;
    if (k == 1) {
        res.push_back(make_pair(iNew, jNew));
    }

    map<ll, pair<int, int>> sums;
    for (int j = 0; j < n; ++j) {
        for (int i = 0; i < j; ++i) {
            sums[2 * (a[i] + a[j])] = pair<int, int>(i, j);
        }
    }

    for (int j = 0; j < m; ++j) {
        for (int i = 0; i < j; ++i) {
            const ll ds = s0 - s1 + 2 * (b[i] + b[j]);
            auto iter = sums.lower_bound(ds);
            if (iter != begin(sums)) --iter;
            for (int id = 0; id < 2; ++id) {
                if (iter == end(sums)) break;
                const ll vCur = abs(ds - iter->first);
                if (vCur < v) {
                    v = vCur;
                    pair<int, int> ij = iter->second;
                    res.clear();
                    res.push_back(make_pair(ij.first, i));
                    res.push_back(make_pair(ij.second, j));
                }
                ++iter;
            }
        }
    }

    cout << v << endl;
    cout << res.size() << endl;
    for (auto p : res) cout << p.first+1 << " " << p.second+1 << endl;

    return 0;
}