#include <iostream>
#include <cstdio>
#include <vector>
#include <map>
using namespace std;

typedef long long ll;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    int n = 0;
    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < n; ++i) cin >> a[i];

    map<int, int> m;
    vector<pair<int, int>> res;
    int start = 0;
    int cur = 0;
    while (cur < n) {
        m[a[cur]]++;
        if (m[a[cur]] == 2) {
            res.push_back(make_pair(start + 1, cur + 1));
            m.clear();
            start = cur + 1;
        }
        ++cur;
    }

    const int len = (int)res.size();
    if (len == 0) {
        cout << "-1" << endl;
        return 0;
    }

    if (!m.empty()) res[res.size() - 1].second = n;
    cout << res.size() << endl;
    for (auto p : res) cout << p.first << " " << p.second << endl;

    return 0;
}