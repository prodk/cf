#include <iostream>
#include <cstdio>
using namespace std;

typedef long long ll;
int v[] = { 6, 2, 5, 5, 4, 5, 6, 3, 7, 6 };

ll solve(int i) {
    ll res = 0;
    while (i > 0) {
        const int rem = i % 10;
        res += v[rem];
        i /= 10;
    }
    return res;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    int a = 0;
    int b = 0;
    cin >> a >> b;

    ll res = 0l;
    for (int i = a; i <= b; ++i) {
        res += solve(i);
    }

    cout << res << endl;

    return 0;
}