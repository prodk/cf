#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
using namespace std;

typedef long long ll;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    pair<int, int> p0;
    cin >> p0.first >> p0.second;
    pair<int, int> p1;
    cin >> p1.first >> p1.second;

    const ll dx = abs(p0.first - p1.first);
    const ll dy = abs(p0.second - p1.second);
    const ll diag = max(dx, dy);

    cout << diag << endl;

    return 0;
}