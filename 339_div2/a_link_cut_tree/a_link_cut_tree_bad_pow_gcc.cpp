#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;

typedef long long ll;

ll l, r, k;

#define DEBUG

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(0);

    cin >> l >> r >> k;

    if (k > r) cout << "-1" << endl;
    else {
        ll val = 1;
        if (l != 1) {
            const ll startPow = static_cast<ll>(log((double)l) / log((double)k));
            val = pow(k, startPow);
            if (val < l) val *= k;
        }
        for (; val <= r; val *= k) cout << val << " ";
        cout << endl;
    }

    return 0;
}