#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;

typedef long long ll;

ll l, r, k;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    cin >> l >> r >> k;

    if (l == 1) cout << "1 ";
    else if (k > r) {
        cout << "-1" << endl;
        return 0;
    }

    double val = (double)k;
    while (val < (double)l) {
        val *= k;
    }
    if (l != 1 && val > r) {
        cout << "-1" << endl;
        return 0;
    }

    for (; val <= r; val *= k) cout << (ll)val << " ";
    cout << endl;

    return 0;
}