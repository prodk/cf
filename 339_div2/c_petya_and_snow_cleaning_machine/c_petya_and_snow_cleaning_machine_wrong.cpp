#include <cstdio>
#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <string>
#include <climits>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    int n = 0;
    cin >> n;
    double x0 = 0;
    double y0 = 0;
    cin >> x0 >> y0;

    const double pi = 4 * atan(1.0);

    vector<pii> v(n);
    for (int i = 0; i < n; ++i) {
        cin >> v[i].first >> v[i].second;
    }

    double rmin2 = (double)INT_MAX;
    double rmax2 = 0;
    for (auto i : v) {
        const double r2 = (x0 - i.first)*(x0 - i.first) + (y0 - i.second)*(y0 - i.second);
        rmin2 = min(rmin2, r2);
        rmax2 = max(rmax2, r2);
    }

    for (int i = 0; i < n; ++i) {
        const int idPrev = (i - 1 + n) % n;
        const double x1 = v[idPrev].first;
        const double y1 = v[idPrev].second;
        const double x2 = v[i].first;
        const double y2 = v[i].second;
        const double dx = x2 - x1;
        const double dy = y2 - y1;
        const double dx0 = x0 - x1;
        const double dy0 = y0 - y1;

        const double denom = dy*dy + dx*dx;
        const double len = sqrt(denom);
        const double dotProduct = (dx*dx0 + dy*dy0)/len;
        if (dotProduct < 0.0 || dotProduct > len)
            continue;

        double nom = dy*x0 - dx*y0 + x2*y1 - y2*x1;
        nom *= nom;
        const double d = nom / denom;
        rmin2 = min(rmin2, d);
        rmax2 = max(rmax2, d);
    }

    const double dr2 = (rmax2 - rmin2);
    const double s = pi*dr2;
    cout << setprecision(18) << fixed << s << endl;

    return 0;
}