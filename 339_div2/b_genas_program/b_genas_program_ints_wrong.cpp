#include <cstdio>
#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip>
#include <algorithm>
using namespace std;

typedef long long ll;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    int n = 0;
    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < n; ++i) cin >> a[i];

    const int ten[] = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000 };
    const int zero[] = { 0, 1, 2, 3, 4, 5, 6, 7};
    const int len = sizeof(ten) / sizeof(ten[0]);

    int p = 0;
    int numOfZeros = 0;
    for (auto i : a) {
        if (i == 0) {
            p = 0;
            break;
        }
        if (binary_search(begin(ten), end(ten), i)) {
            const int id = lower_bound(begin(ten), end(ten), i) - begin(ten);
            numOfZeros += zero[id];
        }
        else {
            p = i;
        }
    }

    if (p == 0) cout << p << endl;
    else {
        cout << p;
        for (int i = 0; i < numOfZeros; ++i) cout << "0";
        cout << endl;
    }

    return 0;
}