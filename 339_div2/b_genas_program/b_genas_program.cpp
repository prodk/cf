#include <cstdio>
#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <string>
using namespace std;

typedef long long ll;

bool isNice(const string & s) {
    if (s.size() == 0) return false;
    if (s[0] != '1') return false;
    for (int i = 1; i < s.size(); ++i)
        if (s[i] != '0') {
            return false;
        }

    return true;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    int n = 0;
    cin >> n;
    vector<string> a(n);
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }

    int pId = -1;
    int numOfZeros = 0;
    for (int i = 0; i < n; ++i) {
        const string & s = a[i];
        if (s.size() == 1 && s[0] == '0') {
            cout << "0" << endl;
            return 0;
        }

        if (isNice(s)) {
            numOfZeros += s.size() - 1;
        }
        else {
            pId = i;
        }
    }

    if (pId != -1)
        cout << a[pId];
    else
        cout << "1";
    for (int i = 0; i < numOfZeros; ++i) cout << "0";
    cout << endl;

    return 0;
}