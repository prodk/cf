#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

#define DEBUG 1

typedef long long ll;

vector<vector<int>> g;
vector<bool> visited;
vector<ll> len;
vector<ll> nice;
//vector<int> path;

void dfs(int v) {
   visited[v] = true;
   for (auto u : g[v]) {
      if (!visited[u] && u > v) {
         len[u] = max(len[u], len[v] + 1);
         const ll newNice = len[u] * g[u].size();
         if (nice[u] < newNice) {
            nice[u] = newNice;
            //path[u] = v;
         }
         dfs(u);
      }
   }
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int n = 0;
   cin >> n;
   int m = 0;
   cin >> m;

   g.resize(n);
   for (int i = 0; i < m; ++i) {
      int u = 0;
      cin >> u;
      int v = 0;
      cin >> v;
      --u;
      --v;
      g[u].push_back(v);
      g[v].push_back(u);
   }
   for (int i = 0; i < n; ++i) {
      sort(begin(g[i]), end(g[i]));
   }

   visited.resize(n);
   fill(begin(visited), end(visited), false);
   len.resize(n);
   fill(begin(len), end(len), 1);
   nice.resize(n);
   fill(begin(nice), end(nice), 1);
   //path.resize(n);
   //fill(begin(path), end(path), -1);

   for (int i = 0; i < n; ++i) {
      if (!visited[i]) {
         dfs(i);
      }
   }

   cout << *max_element(begin(nice), end(nice));

   return 0;
}