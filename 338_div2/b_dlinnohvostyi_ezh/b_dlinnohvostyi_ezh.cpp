#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

#define DEBUG 1

typedef long long ll;

vector<vector<int>> g;
vector<ll> len;

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int n = 0;
   cin >> n;
   int m = 0;
   cin >> m;

   g.resize(n);
   for (int i = 0; i < m; ++i) {
      int u = 0;
      cin >> u;
      int v = 0;
      cin >> v;
      --u;
      --v;
      g[u].push_back(v);
      g[v].push_back(u);
   }

   len.resize(n);
   fill(begin(len), end(len), 1);

   for (int v = 0; v < n; ++v) {
      for (auto u : g[v]) {
         if (u > v) {
            len[u] = max(len[u], len[v] + 1);
         }
      }
   }

   ll res = 0;
   for (int v = 0; v < n; ++v) {
      res = max(res, len[v] * g[v].size());
   }
   cout << res << endl;

   return 0;
}