#include <iostream>
#include <cstdio>
#include <vector>
#include <cmath>
using namespace std;

typedef long long ll;

//#define DEBUG

int main()
{
#ifdef DEBUG
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
#endif

    ll n = 0;
    cin >> n;

    const double root = sqrt(1. + 4.*n / 3.);
    ll turn = static_cast<ll>(0.5*(-1. + root)) - 1;
    ll si = 3 * turn * (turn + 1);

    //cout << "0 si " << si << " " << turn << " root " << root << endl;

    for (ll nextTurn = turn + 1; si <= n; ++nextTurn) {
        const ll nextSi = si + nextTurn * 6;
        if (nextSi <= n) {
            si = nextSi;
            turn = nextTurn;
        }
        else break;
    }

    const ll delta = n - si;
    ll resX = 0;
    ll resY = 0;
    if (0 == delta) {
        resX = 2 * turn;
        resY = 0;
    }
    else {
        const ll edgeLen = turn + 1;
        const ll corner = delta / edgeLen;
        const ll edgesToCorner = delta - corner*edgeLen;

        //cout << "1 si " << si << " " << turn << endl;

        ll cornerX = 0;
        ll cornerY = 0;
        switch (corner) {
        case 0:
            cornerX = 2 * (turn + 1); cornerY = 0;
            resX = cornerX - edgesToCorner;
            resY = 2 * edgesToCorner;
            break;

        case 1:
            cornerX = turn + 1; cornerY = 2 * (turn + 1);
            resX = cornerX - 2 * edgesToCorner;
            resY = cornerY;
            break;

        case 2:
            cornerX = -(turn + 1); cornerY = 2 * (turn + 1);
            resX = cornerX - edgesToCorner;
            resY = cornerY - 2 * edgesToCorner;
            break;

        case 3:
            cornerX = -2 * (turn + 1); cornerY = 0;
            resX = cornerX + edgesToCorner;
            resY = cornerY - 2 * edgesToCorner;
            break;

        case 4:
            cornerX = -(turn + 1); cornerY = -2 * (turn + 1);
            resX = cornerX + 2 * edgesToCorner;
            resY = cornerY;
            break;

        case 5:
            cornerX = turn + 1; cornerY = -2 * (turn + 1);
            resX = cornerX + edgesToCorner;
            resY = cornerY + 2 * edgesToCorner;
            break;
        }

    }

    cout << resX << " " << resY << endl;

    return 0;
}