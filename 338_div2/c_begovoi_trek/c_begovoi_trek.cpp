#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <iostream>
#define maxn 2200
#define inf 0x3f3f3f3f
using namespace std;
typedef long long ll;
typedef pair<int, int> pii;
char s[maxn], s2[maxn], t[maxn];
int lcp1[maxn][maxn], lcp2[maxn][maxn];
int dp[maxn], fa[maxn];
pii path[maxn];
vector<pii>ans;
int main()
{
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
   memset(dp, inf, sizeof(dp));
   scanf("%s %s", s + 1, t + 1);
   int len1 = strlen(s + 1); int len2 = strlen(t + 1);
   for (int i = 1; i <= len1; i++)
      s2[i] = s[len1 - i + 1];
   for (int i = 1; i <= len1; i++)
      for (int j = 1; j <= len2; j++)
         if (s[i] == t[j]) lcp1[i][j] = lcp1[i - 1][j - 1] + 1;
   for (int i = 1; i <= len1; i++)
      for (int j = 1; j <= len2; j++)
         if (s2[i] == t[j]) lcp2[i][j] = lcp2[i - 1][j - 1] + 1;
   dp[0] = 0;
   for (int i = 1; i <= len2; i++) {
      for (int j = 1; j <= len1; j++) {
         if (dp[i]>dp[i - lcp1[j][i]] + 1) {
            int pos1 = j; int pos2 = j - lcp1[j][i] + 1;
            path[i] = make_pair(pos2, pos1);
            fa[i] = i - lcp1[j][i];
            dp[i] = dp[i - lcp1[j][i]] + 1;
         }
         if (dp[i]>dp[i - lcp2[j][i]] + 1) {
            int pos1 = len1 - j + 1; int pos2 = pos1 + lcp2[j][i] - 1;
            path[i] = make_pair(pos2, pos1);
            fa[i] = i - lcp2[j][i];
            dp[i] = dp[i - lcp2[j][i]] + 1;
         }
      }
   }
   if (dp[len2] >= inf) puts("-1");
   else {
      printf("%d\n", dp[len2]);
      int pos = len2;
      while (pos) {
         ans.push_back(path[pos]);
         pos = fa[pos];
      }
      int Size = ans.size();
      for (int i = Size - 1; i >= 0; i--)
         cout << ans[i].first << " " << ans[i].second << endl;
   }
}