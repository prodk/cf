#include <iostream>
#include <set>
using namespace std;

int main()
{
    int n = 0;
    cin >> n;
    int m = 0;
    cin >> m;
    
    set<int> s;
    for (int i = 0; i < n; ++i) {
        int k = 0;
        cin >> k;
        //cout << "k" << k << endl;
        for (int j = 0; j < k; ++j) {
            int a = 0;
            cin >> a;
            //cout << a << " ";
            s.insert(a);
        }
        //cout << endl;
    }
    
    //cout << s.size() << endl;
    
    if (s.size() == m) cout << "YES" << endl;
    else cout << "NO" << endl;
    
    return 0;
}